import QtQuick 2.15
import Qt5Compat.GraphicalEffects

Item {
    id: rootGauge

    height: 620
    width: 620

    property int speed: 0
    property int animationDelay: 40

    Image {
        id: gaugeImage

        source: "./assets/gaugeBlue.png"
        anchors.fill: parent
    }

    Needle {
        id: gaugeNeedle

        height: 300
        width: 90
        y: gaugeImage.height/2
        anchors.horizontalCenter: gaugeImage.horizontalCenter

        needleRed: rootGauge.speed >= 6500 - 45 //Image width

        transform: Rotation {
            id: needleRotation

            origin.y: 0
            origin.x: gaugeNeedle.width/2

            angle: Math.min(rootGauge.speed/29.73 + 45, 313.75) > 45.25 ? Math.min(rootGauge.speed/29.73 + 45, 313.75) : 45.25

            Behavior on angle {
                    NumberAnimation {duration: 40}
            }
        }

    }

}
