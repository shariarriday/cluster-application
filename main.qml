import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id: rootWindow

    property int heightFixed: 720
    property int widthFixed: 1920

    property int speed: 0
    property int speedDirection: 1
    property int accelaration: 5

//    property int frameTime: 0
//    property real startTime: Date.now()

    minimumWidth: rootWindow.widthFixed
    minimumHeight: rootWindow.heightFixed
    //    maximumWidth: widthFixed
    //    maximumHeight: heightFixed

    visible: true

    title: qsTr("Cluster")

    onFrameSwapped: {
//        rootWindow.frameTime = Date.now() - rootWindow.startTime;
//        rootWindow.startTime = Date.now();
        if(frameMeter.visible) frameMeter.addNewValue();
    }

    Timer {
        id: timerRPM
        interval: 40 //1 Km
        running: false
        repeat: true
        onTriggered: {
            if(rootWindow.speedDirection == 1 && rootWindow.speed < 8000)
                rootWindow.speed = rootWindow.speed + rootWindow.accelaration;
            else if(rootWindow.speedDirection == -1 && rootWindow.speed > 0)
                rootWindow.speed = rootWindow.speed - rootWindow.accelaration;
        }
    }

    Timer {
        id: accTimer

        interval: 1000 //1 Second
        running: false
        repeat: true

        onTriggered: rootWindow.accelaration > 80 ? 80 : rootWindow.accelaration *= 1.8
    }

    Image {
        id: backgroundImagee
        source: "./assets/backgroundBlue.png"
        anchors.fill: parent

        focus: true

        Gauge {
            id: gaugeComponent
            anchors.centerIn: parent
            speed: rootWindow.speed
            animationDelay: rootWindow.accelaration
        }

        FontLoader { id: customFont; source: "./assets/DejaVuSansMono-Bold.ttf" }

        Text {
            id: speedText

            anchors.centerIn: backgroundImagee

            font.family: customFont.name
            font.pointSize: 60
            color: "white"
            text: parseInt(Math.min(200, Math.max(0, rootWindow.speed/40))) + "\nkm\\h"
            horizontalAlignment: Text.AlignHCenter
        }

//        Text {
//            id: fpsText

//            anchors.right: parent.right
//            anchors.top: parent.top

//            text: "FrameTime: " + rootWindow.frameTime
//            font.family: customFont.name
//            font.pointSize: 10
//            color: "white"
//            horizontalAlignment: Text.AlignHCenter
//        }

        FrameChart {
            id: frameMeter

            anchors.left: parent.left
            anchors.top: parent.top

            height: 200
            width: 500

            visible: true
        }

        Keys.onPressed: function (event) {
            if (event.key === Qt.Key_Up) {
                timerRPM.running = true;
                accTimer.running = true;
                rootWindow.speedDirection = 1;
                event.accepted = true;
            }

            if (event.key === Qt.Key_Down) {
                timerRPM.running = true;
                accTimer.running = true;
                rootWindow.speedDirection = -1;
                event.accepted = true;
            }
        }

        Keys.onReleased: function (event) {
            if (!event.isAutoRepeat)
            {
                if (event.key === Qt.Key_Up) {
                    timerRPM.running = false;
                    accTimer.running = false;
                    rootWindow.accelaration = 5;
                    event.accepted = true;
                }

                if (event.key === Qt.Key_Down) {
                    timerRPM.running = false;
                    accTimer.running = false;
                    rootWindow.accelaration = 5;
                    event.accepted = true;
                }
            }
        }
    }
}
