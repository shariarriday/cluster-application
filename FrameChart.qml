import QtQuick 2.15
import QtCharts 2.15
import frame.timeGraph.component 1.0

Item {
    id: chartItem

    function addNewValue(yVal) {
        frameTimeSeries.appendDataToChart();
    }

    ChartView {
        id: chart

        anchors.fill: parent

        antialiasing: true
        theme: ChartView.ChartThemeDark

        FrameTime {
            id: frameTimeSeries
        }

        ValuesAxis {
            id: xValueAxis
            labelsVisible: false
            max: frameTimeSeries.limit
            min: frameTimeSeries.limit-120
        }

        ValuesAxis {
            id: yValueAxis
            max: 100
            min: 1
        }

        Component.onCompleted: {
            chart.setAxisX(xValueAxis, frameTimeSeries)
            chart.setAxisY(yValueAxis, frameTimeSeries)
        }
    }

}
