#version 440
layout(location = 0) in vec2 coord;
layout(location = 0) out vec4 fragColor;
layout(std140, binding = 0) uniform buf {
    mat4 qt_Matrix;
    float qt_Opacity;
};
layout(binding = 1) uniform sampler2D src;
void main() {
    vec4 tex = texture(src, coord);
    lowp float avg = (tex.r + tex.g + tex.b) / 3.;
    lowp float avgWithoutGreen = (tex.r + tex.b) / 2.;
    if(avgWithoutGreen >= avg)
        fragColor = vec4(vec3(tex.r, tex.g, tex.b), tex.a) * qt_Opacity;
    else
    {
        fragColor = vec4(vec3(1*avg, .0*avg, .0*avg), tex.a) * qt_Opacity;
    }
}
