#include "frametimegraphdata.h"

FrameTimeGraphData::FrameTimeGraphData():
    m_lastFrameTime(QDateTime::currentMSecsSinceEpoch())
{
    this->currentCount = 0;
    this->currentRemove = 0;
    this->avgTime = 1000.0/60.0;
    this->setColor(QColor("black"));
    this->setName(QString("Frame Time(Ms)"));
}



void FrameTimeGraphData::appendDataToChart()
{
    int currentTime = QDateTime::currentMSecsSinceEpoch();
    this->avgTime = (this->avgTime + (currentTime- this->m_lastFrameTime) )/2;
    this->append(this->currentCount, this->avgTime);
    this->m_lastFrameTime = currentTime;
    emit pointAdded( (this->currentCount - this->currentRemove));
    if(currentCount > 120)
    {
        this->remove(0);
        this->currentRemove++;
    }
    this->setLimit(this->currentCount+1);
}



int FrameTimeGraphData::getLimit() const
{
    return this->currentCount;
}



void FrameTimeGraphData::setLimit(int val)
{
    if(this->currentCount == val)
        return;

    this->currentCount = val;
    emit limitChanged();
}


//QAbstractAxis* FrameTimeGraphData::getAxisX()
//{
//    return m_xAxis;
//}

//QAbstractAxis* FrameTimeGraphData::getAxisY()
//{
//    return m_yAxis;
//}

//QAbstractAxis* FrameTimeGraphData::getAxisXTop()
//{
//    return m_xAxisTop;
//}

//QAbstractAxis* FrameTimeGraphData::getAxisYRight()
//{
//    return m_yAxisRight;
//}

//void FrameTimeGraphData::setAxisXTop(QAbstractAxis *axis)
//{
//    if(m_xAxisTop == axis)
//        return;

//    delete m_xAxisTop;
//    m_xAxisTop = axis;
//    emit axisXTopChanged(m_xAxisTop);
//}

//void FrameTimeGraphData::setAxisY(QAbstractAxis *axis)
//{
//    if(m_yAxis == axis)
//        return;

//    delete m_yAxis;
//    m_yAxis = axis;
//    emit axisYChanged(m_yAxis);
//}

//void FrameTimeGraphData::setAxisYRight(QAbstractAxis *axis)
//{
//    if(m_yAxisRight == axis)
//        return;

//    delete m_yAxisRight;
//    m_yAxisRight = axis;
//    emit axisYRightChanged(m_yAxisRight);
//}
