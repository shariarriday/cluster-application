#ifndef FRAMETIMEGRAPHDATA_H
#define FRAMETIMEGRAPHDATA_H

#include <QObject>
#include <QLineSeries>
#include <QDateTime>
#include <QAbstractAxis>

class FrameTimeGraphData: public QLineSeries
{
    Q_OBJECT

    Q_PROPERTY(int limit READ getLimit WRITE setLimit NOTIFY limitChanged)
    //    Q_PROPERTY(QAbstractAxis* axisY READ getAxisY WRITE setAxisY NOTIFY axisYChanged)
    //    Q_PROPERTY(QAbstractAxis* axisXTop READ getAxisXTop WRITE setAxisXTop NOTIFY axisXTopChanged)
    //    Q_PROPERTY(QAbstractAxis* axisYRight READ getAxisYRight WRITE setAxisYRight NOTIFY axisYRightChanged)

public:
    FrameTimeGraphData();
    int getLimit() const;
    //    QAbstractAxis* getAxisY();
    //    QAbstractAxis* getAxisXTop();
    //    QAbstractAxis* getAxisYRight();
    void setLimit(int val);
    //    void setAxisY(QAbstractAxis* axis);
    //    void setAxisXTop(QAbstractAxis* axis);
    //    void setAxisYRight(QAbstractAxis* axis);
signals:
    void limitChanged();
    void axisXChanged(QAbstractAxis*);
    void axisXTopChanged(QAbstractAxis*);
    void axisYChanged(QAbstractAxis*);
    void axisYRightChanged(QAbstractAxis*);

public:
    Q_INVOKABLE void appendDataToChart();

private:
    int m_lastFrameTime;
    int currentCount;
    int currentRemove;
    double avgTime;
    //    QAbstractAxis* m_xAxis;
    //    QAbstractAxis* m_yAxis;
    //    QAbstractAxis* m_xAxisTop;
    //    QAbstractAxis* m_yAxisRight;
};

#endif // FRAMETIMEGRAPHDATA_H
