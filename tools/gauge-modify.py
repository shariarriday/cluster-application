import cv2 as cv
import numpy as np

# Load the aerial image and convert to HSV colourspace
image = cv.imread("../assets/gauge.png", cv.IMREAD_UNCHANGED)

*_, alpha = cv.split(image)

for i in range(0, image.shape[0]):
    for j in range(0, image.shape[1]):
        if image[i,j,0] >= 100 and image[i,j,0] <= 130 and image[i,j,1] >= 100 and image[i,j,1] <= 130 and image[i,j,2] >= 40 and image[i,j,2] <= 55:
            image[i,j,0] += 20
            image[i,j,1] -= 25
            image[i,j,2] += 30
            print(image[i,j])

cv.imwrite("../assets/gaugeBlue.png",image)