import cv2 as cv
import numpy as np

# Load the aerial image and convert to HSV colourspace
image = cv.imread("../assets/background.png", cv.IMREAD_UNCHANGED)

*_, alpha = cv.split(image)

for i in range(0, image.shape[0]):
    for j in range(0, image.shape[1]):
        if image[i,j,0] > 5 and image[i,j,1] > 5 and image[i,j,2] > 5:
            image[i,j,0] = int(image[i,j,0] * 1.4)
            image[i,j,1] = int(image[i,j,1] * 1.1)
            image[i,j,2] = int(image[i,j,2] * 1.6)

cv.imwrite("../assets/backgroundBlue.png",image)