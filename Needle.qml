import QtQuick 2.15

Item {
    id: needle

    property bool needleRed: false
    property color redColor: Qt.color("red")

    Image {
        id: needleImage
        source: "./assets/needle.png"
        anchors.centerIn: parent
        sourceSize.width: 90
        sourceSize.height: 300

        visible: !needle.needleRed

    }
    Image {
        id: needleImageRed
        source: "./assets/needleRed.png"
        anchors.centerIn: parent
        sourceSize.width: 90
        sourceSize.height: 300

        visible: needle.needleRed
    }
}
